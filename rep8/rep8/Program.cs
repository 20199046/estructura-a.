﻿using System;

namespace rep8
{
    class Program
    {
        static void Main(string[] args)
        {
            string dato = string.Empty;

            Console.Write("Ingrese Dato: ");
            dato = Console.ReadLine();

            for (int i = 0; i < dato.Length; i++)
            {
                if (Char.IsPunctuation((char)dato[i]))
                    Console.WriteLine("{0} es una un signo de puntuacion.", dato[i]);

                else if (Char.IsNumber((char)dato[i]))
                    Console.WriteLine("{0} es un número.", dato[i]);

                else if (Char.IsLetterOrDigit((char)dato[i]))
                    Console.WriteLine("{0} es un Digito.", dato[i]);

            }
            Console.ReadLine();
        }
    }
}
