﻿using System;

namespace rep2
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1 = 0;
            int num2 = 0;
            bool result;

            Program ob = new Program();

            Console.Write("Introduce el primer número: ");
            num1 = int.Parse(Console.ReadLine());

            Console.Write("Introduce el segundo número: ");
            num2 = int.Parse(Console.ReadLine());

            result = ob.esMultiplo(num1, num2);

            Console.WriteLine();
            Console.WriteLine("{0} {1} de {2}", num1, (result ? "es múltiplo" : "no es múltiplo"), num2);
            Console.Write("Pulse una Tecla:");
            Console.ReadLine();
        }

        /**
         * Función que dado dos numeros enteros, determina si son multiplos
         * Devuelve true o false
         */
        private bool esMultiplo(int n1, int n2)
        {
            if (n1 % n2 == 0)
            {
                return true;
            }
            return false;
        }
    }
}
