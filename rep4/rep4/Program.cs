﻿using System;

namespace rep4
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero;
            int numero2;
            int result;

            Console.WriteLine("Ingrese numero: ");
            numero = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Ingrese otro numero: ");
            numero2 = Convert.ToInt32(Console.ReadLine());

            if (numero2 > 0)
            {
                result = numero / numero2;
                Console.WriteLine("Este es el resultado: " + result);
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Error no se puede dividir entre 0 ");
                Console.ReadKey();
            }
        }
    }
}
